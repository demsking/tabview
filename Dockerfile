FROM ubuntu:16.04

USER root

# Configure Neon Registry
ADD neon/public.key /
ADD neon/neon.list /etc/apt/sources.list.d/

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections \
  && apt-key add /public.key \
  && rm /public.key \
  && apt-get update --fix-missing

# Install Qt/KDE libraries
RUN apt-get -y install g++ make kdelibs5-dev libkf5kdelibs4support-dev \
    qt5-default qt5-qmake libkf5texteditor-dev libkf5configcore5 \
    libpackagekitqt5-dev cmake extra-cmake-modules gettext \
    appstream git curl cppcheck \
  && rm -r /var/lib/apt/lists/* \
  && apt-get clean \
  && apt-get -y autoremove \
  && apt-get update

# Install NPM Packages
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - \
  && apt-get install -y nodejs \
  && npm install -g stylus

RUN useradd -G root -ms /bin/bash dev

USER dev

ENV HOME=/home/dev

WORKDIR /home/dev

CMD ["/bin/bash"]
