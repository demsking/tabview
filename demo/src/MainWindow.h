/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <QMainWindow>

namespace HPX {
  class Tabview;
  class TabviewModel;
}

class MainWindow: public QMainWindow {
  Q_OBJECT

  HPX::Tabview* _tabview = nullptr;
  HPX::TabviewModel* _model = nullptr;

  public:
    explicit MainWindow(const char* theme);
    ~MainWindow() = default;

    void loadTheme(const QString& filename);
};

#endif  // MAINWINDOW_H_
