﻿/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Document.h"

#include <QDir>
#include <QColor>

#include <KTextEditor/Document>
#include <KTextEditor/Editor>
#include <KTextEditor/View>
#include <KTextEditor/ConfigInterface>

#include <KSharedConfig>
#include <KConfigGroup>

#include <KStandardAction>

#define CONFIG_KEY_SELECTION_RANGE "Selection Range"
#define CONFIG_KEY_CURSOR_POSITION "Cursor Position"
#define CONFIG_KEY_FIRST_DISPLAYED_LINE "First Displayed Line"

#define VIEW_CONFIG_KEY_BACKGROUND_COLOR "background-color"
#define VIEW_CONFIG_KEY_SELECTION_COLOR "selection-color"
#define VIEW_CONFIG_KEY_SEARCH_HIGHLIGHT_COLOR "search-highlight-color"
#define VIEW_CONFIG_KEY_REPLACE_HIGHLIGHT_COLOR "replace-highlight-color"
#define VIEW_CONFIG_KEY_ICON_BORDER_COLOR "icon-border-color"
#define VIEW_CONFIG_KEY_FOLDING_MARKER_COLOR "folding-marker-color"
#define VIEW_CONFIG_KEY_LINE_NUMBER_COLOR "line-number-color"
#define VIEW_CONFIG_KEY_CURRENT_LINE_NUMBER_COLOR "current-line-number-color"
#define VIEW_CONFIG_KEY_FONT "font"
#define VIEW_CONFIG_KEY_SCHEME "scheme"

namespace CodeLineX {
  Document::Document(QWidget* parent): TabviewWidget(parent) {
    layout = new QVBoxLayout();

    layout->setMargin(0);
    layout->setSpacing(0);

    setLayout(layout);
    setProperty("Editor", true);

    // create a new document with nullptr as parent.
    // this is needed because the created doc pointer may be shared
    // between many other Documents, so it should be deleted only
    // when the last Document itself is deleted.
    setDoc(KTextEditor::Editor::instance()->createDocument(nullptr));
  }

  Document::Document(const QUrl& url, QWidget* parent): Document(parent) {
    openFile(url);
  }

  Document::~Document() {
    if (view) {
      delete view;
    }

    if (doc->views().isEmpty()) {
      doc->deleteLater();
    }

    delete layout;
  }

  QString Document::name() const {
    if (doc->isModified()) {
      return doc->documentName() + " *";
    }

    return doc->documentName();
  }

  QUrl Document::url() const {
    return doc->url();
  }

  QString Document::path() const {
    return url().toLocalFile();
  }

  QString Document::encoding() const {
    return doc->encoding();
  }

  QString Document::endOfLine() const {
    return doc->endOfLine(1).toString();
  }

  QString Document::mimeType() const {
    return doc->mimeType();
  }

  QString Document::mode() const {
    return doc->mode();
  }

  Document::InputMode Document::viewInputMode() const {
    return view->viewInputMode();
  }

  QString Document::viewInputModeHuman() const {
    return view->viewInputModeHuman();
  }

  Document::ViewMode Document::viewMode() const {
    return view->viewMode();
  }

  QString Document::viewModeHuman() const {
    return view->viewModeHuman();
  }

  Document::Cursor Document::cursorPosition() const {
    return view->cursorPosition();
  }

  bool Document::isModified() const {
    return doc->isModified();
  }

  bool Document::isReadOnly() const {
    return doc->isReadWrite();
  }

  void Document::setWritable(bool readwrite) {
    doc->setReadWrite(readwrite);
  }

  const QFont& Document::font() const {
    return _font;
  }

  void Document::setFont(const QFont& font) {
    _font = font;
    configView->setConfigValue(VIEW_CONFIG_KEY_FONT, font);
  }

  void Document::increaseFontSize(qreal step) {
    _font.setPointSizeF(_font.pointSizeF() + step);

    configView->setConfigValue(VIEW_CONFIG_KEY_FONT, _font);
  }

  void Document::decreaseFontSize(qreal step) {
    if ((_font.pointSizeF() - step) > 0) {
      _font.setPointSizeF(_font.pointSizeF() - step);
    }

    configView->setConfigValue(VIEW_CONFIG_KEY_FONT, _font);
  }

  void Document::resetFontSize() {
    setFont(_initialFont);
  }

  QList<KTextEditor::View*> Document::views() const {
    return doc->views();
  }

  QColor Document::backgroundColor() const {
    return configView->configValue(VIEW_CONFIG_KEY_BACKGROUND_COLOR).value<QColor>();
  }

  QColor Document::selectionColor() const {
    return configView->configValue(VIEW_CONFIG_KEY_SELECTION_COLOR).value<QColor>();
  }

  QColor Document::searchHighlightColor() const {
    return configView->configValue(VIEW_CONFIG_KEY_SEARCH_HIGHLIGHT_COLOR).value<QColor>();
  }

  QColor Document::replaceHighlightColor() const {
    return configView->configValue(VIEW_CONFIG_KEY_REPLACE_HIGHLIGHT_COLOR).value<QColor>();
  }

  QColor Document::iconBorderColor() const {
    return configView->configValue(VIEW_CONFIG_KEY_ICON_BORDER_COLOR).value<QColor>();
  }

  QColor Document::foldingMarkerColor() const {
    return configView->configValue(VIEW_CONFIG_KEY_FOLDING_MARKER_COLOR).value<QColor>();
  }

  QColor Document::lineNumberColor() const {
    return configView->configValue(VIEW_CONFIG_KEY_LINE_NUMBER_COLOR).value<QColor>();
  }

  QColor Document::currentLineNumberColor() const {
    return configView->configValue(VIEW_CONFIG_KEY_CURRENT_LINE_NUMBER_COLOR).value<QColor>();
  }

  QString Document::scheme() const {
    return configView->configValue(VIEW_CONFIG_KEY_SCHEME).toString();
  }

  QAction* Document::action(Document::Action id) const {
    QString name;

    switch (id) {
      case Action::DYNAMIC_WORD_WRAP:
        name = "view_dynamic_word_wrap";
        break;

      case Action::DYNAMIC_WORD_WRAP_INDICATOR:
        name = "dynamic_word_wrap_indicators";
        break;

      case Action::DYNAMIC_WORD_WRAP_MARKER:
        name = "view_word_wrap_marker";
        break;

      case Action::EXPORT_HTML:
        name = "file_export_html";
        break;

      case Action::RELOAD:
        name = "file_reload";
        break;

      case Action::SAVE_AS_WITH_ENCODING:
        name = "file_save_as_with_encoding";
        break;

      case Action::SHOW_NON_PRINTABLE_SPACES:
        name = "view_non_printable_spaces";
        break;

      case Action::ZOOM_IN:
        name = "view_inc_font_sizes";
        break;

      case Action::ZOOM_OUT:
        name = "view_dec_font_sizes";
        break;
    }

    return view->action(name.toStdString().c_str());
  }

  QAction* Document::action(const StandardAction id) const {
    return view->action(KStandardAction::name(id));
  }

  Document::ActionCollection* Document::actionCollection() const {
    return view->actionCollection();
  }

  QVector<Document::Range> Document::searchText(const Document::Range& range, const QString& pattern,
                                                const Document::SearchOptions options) const {
    return doc->searchText(range, pattern, options);
  }

  KTextEditor::View* Document::createView(QWidget* parent) {
    return doc->createView(parent);
  }

  bool Document::reload() {
    return doc->documentReload();
  }

  void Document::setDoc(KTextEditor::Document* value) {
    if (view) {
      delete view;

      doc->disconnect(this);
      doc->closeUrl();
      doc->deleteLater();
    }

    doc = value;
    view = doc->createView(this);

    configView = qobject_cast<KTextEditor::ConfigInterface*>(view);
    configDoc = qobject_cast<KTextEditor::ConfigInterface*>(doc);

    _initialFont = configView->configValue(VIEW_CONFIG_KEY_FONT).value<QFont>();
    _font = _initialFont;

    view->setProperty("EditorView", true);
    view->setStatusBarEnabled(false);

    setFocusProxy(view);
    disableViewActionShortcuts();
    layout->addWidget(view);

    configView->setConfigValue("line-numbers", true);
    configView->setConfigValue("icon-bar", false);
    configView->setConfigValue("folding-bar", true);
    configView->setConfigValue("folding-preview", true);
    configView->setConfigValue("dynamic-word-wrap", false);
    configView->setConfigValue("modification-markers", true);
    configView->setConfigValue("word-count", false);
    configView->setConfigValue("scrollbar-minimap", true);
    configView->setConfigValue("scrollbar-preview", true);
    configView->setConfigValue("allow-mark-menu", true);
//    configView->setConfigValue("font", QFont());

    connect(view, &KTextEditor::View::focusIn, this, [this]() {
      emit focusIn();
    });

    connect(view, &KTextEditor::View::focusOut, this, [this]() {
      emit focusOut();
    });

    connect(view, &KTextEditor::View::cursorPositionChanged, this, [this](auto, auto pos) {
      emit this->cursorPositionChanged(this, pos);
    });

    connect(view, &KTextEditor::View::viewModeChanged, this, [this](auto, auto mode) {
      emit this->viewModeChanged(this, mode);
    });

    connect(view, &KTextEditor::View::viewInputModeChanged, this, [this]() {
      emit viewInputModeChanged(this, view->viewInputModeHuman());
    });

    connect(doc, &KTextEditor::Document::documentSavedOrUploaded, this, [this]() {
      emit saved(this);
    });

    connect(doc, &KTextEditor::Document::documentUrlChanged, this, [this]() {
      emit urlChanged(url());
    });

    connect(doc, &KTextEditor::Document::documentNameChanged, this, [this]() {
      emit nameChanged(name());
    });

    connect(doc, &KTextEditor::Document::modifiedChanged, this, [this]() {
      emit nameChanged(name());
      emit modifiedChanged(this);
    });
  }

  void Document::disableViewActionShortcuts() {
    static const QList<KStandardAction::StandardAction> actions = {
      KStandardAction::Save,
      // Edit Menu
      KStandardAction::Undo,
      KStandardAction::Redo,
      KStandardAction::Cut,
      KStandardAction::Copy,
      KStandardAction::Copy,
      KStandardAction::Paste,
      KStandardAction::SelectAll,
      KStandardAction::Deselect,
      KStandardAction::Find,
      KStandardAction::Replace
    };

    foreach (auto action, actions) {
//      view->action(KStandardAction::name(action))->setShortcut(QKeySequence::UnknownKey);
    }

//    action(Action::RELOAD)->setDisabled(true);
//    action(Action::ZOOM_IN)->setDisabled(true);
//    action(Action::ZOOM_OUT)->setDisabled(true);
  }

  void Document::setBackgroundColor(const QColor& value) {
    configView->setConfigValue(VIEW_CONFIG_KEY_BACKGROUND_COLOR, value);
  }

  void Document::setSelectionColor(const QColor& value) {
    configView->setConfigValue(VIEW_CONFIG_KEY_SELECTION_COLOR, value);
  }

  void Document::setSearchHighlightColor(const QColor& value) {
    configView->setConfigValue(VIEW_CONFIG_KEY_SEARCH_HIGHLIGHT_COLOR, value);
  }

  void Document::setReplaceHighlightColor(const QColor& value) {
    configView->setConfigValue(VIEW_CONFIG_KEY_REPLACE_HIGHLIGHT_COLOR, value);
  }

  void Document::setIconBorderColorColor(const QColor& value) {
    configView->setConfigValue(VIEW_CONFIG_KEY_ICON_BORDER_COLOR, value);
  }

  void Document::setFoldingMarkerColor(const QColor& value) {
    configView->setConfigValue(VIEW_CONFIG_KEY_FOLDING_MARKER_COLOR, value);
  }

  void Document::setLineNumberColor(const QColor& value) {
    configView->setConfigValue(VIEW_CONFIG_KEY_LINE_NUMBER_COLOR, value);
  }

  void Document::setCurrentLineNumberColor(const QColor& value) {
    configView->setConfigValue(VIEW_CONFIG_KEY_CURRENT_LINE_NUMBER_COLOR, value);
  }

  void Document::setScheme(const QString& value) {
    configView->setConfigValue(VIEW_CONFIG_KEY_SCHEME, value);
    view->render(this);
  }

  bool Document::setEncoding(const QString& value) {
    return doc->setEncoding(value);
  }

  void Document::openFile(const QUrl& url) {
    KTextEditor::Document* cachedDoc = Document::document(url);

    if (cachedDoc) {
      setDoc(cachedDoc);
    } else {
      doc->openUrl(url);
    }
  }

  void Document::saveFile() {
    doc->documentSave();
  }

  void Document::saveFileAs(const QString& fileName) {
    doc->saveAs(QUrl::fromLocalFile(fileName));
  }

  void Document::close() {
    delete view;

    if (doc->views().isEmpty()) {
      closeFile();
    } else {
      view = nullptr;

      doc->disconnect(this);

      emit closed();
    }
  }

  void Document::closeFile() {
    for (auto view: doc->views()) {
      delete view;
    }

    view = nullptr;

    doc->disconnect(this);
    doc->closeUrl(true);

    emit closed();
  }

  KTextEditor::Document* Document::document(const QUrl& url) {
    for (auto doc: KTextEditor::Editor::instance()->documents()) {
      if (doc->url() == url) {
        return doc;
      }
    }

    return nullptr;
  }
}  // namespace CodeLineX
