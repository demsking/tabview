/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MainWindow.h"

#include <QFile>
#include <QFileSystemWatcher>

#include "Document.h"
#include "models/TabviewItem.h"
#include "models/TabviewModel.h"
#include "components/Tabview.h"

MainWindow::MainWindow(const char* theme): QMainWindow() {
  setWindowTitle("HPX Tabview Demo");
  setGeometry(892, 342, 760, 650);

  _tabview = new HPX::Tabview(this);
  _model = new HPX::TabviewModel(this);

  _tabview->setModel(_model);

  setCentralWidget(_tabview);

  QList<QString> urls = {
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/main.cpp",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/MainWindow.h",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/themes/dark.styl",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/themes/mixins.styl",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/MainWindow.cpp",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/components/Document.h",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/components/Document.cpp",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/components/CloseButton.h",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/components/CloseButton.cpp",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/components/Tabview.h",
    "file:///home/demsking/Workspace/projects/CodeLineLab/src/components/Tabview.cpp"
  };

  for (const QString& filename: urls) {
    CodeLineX::Document* document = new CodeLineX::Document(QUrl(filename), this);
    HPX::TabviewItem* item = new HPX::TabviewItem();

    item->setTitle(document->name());
    item->setToolTip(document->path());
    item->setWidget(document);

    _model->add(item);

    connect(document, &CodeLineX::Document::nameChanged, this, [item](const QString& name) {
      item->setTitle(name);
    });

    connect(document, &CodeLineX::Document::urlChanged, this, [item](const QUrl& url) {
      item->setToolTip(url.toLocalFile());
    });

    connect(document, &CodeLineX::Document::closed, this, [this, document]() {
      document->disconnect(this);
    });
  }

  QFileSystemWatcher* themeWatcher = new QFileSystemWatcher({ theme }, this);

  loadTheme(theme);
  connect(themeWatcher, &QFileSystemWatcher::fileChanged, this, &MainWindow::loadTheme);
}

void MainWindow::loadTheme(const QString& filename) {
  QFile stylesheet(filename);
  stylesheet.open(QFile::ReadOnly);

  QString rules = QLatin1String(stylesheet.readAll());
  setStyleSheet(rules);
}
