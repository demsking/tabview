﻿/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HPXTABVIEW_COMPONENTS_DOCUMENT_H_
#define HPXTABVIEW_COMPONENTS_DOCUMENT_H_

#include <QDir>
#include <QWidget>
#include <QVBoxLayout>

#include <KStandardAction>
#include <KActionCollection>
#include <KTextEditor/Document>
#include <KTextEditor/View>
#include <KConfigGroup>
#include <KTextEditor/ConfigInterface>

#include "hpx-tabview/TabviewWidget.h"

namespace CodeLineX {
  class Document: public HPX::TabviewWidget {
    Q_OBJECT

    protected:
      QVBoxLayout* layout = nullptr;

      KTextEditor::Document* doc = nullptr;
      KTextEditor::View* view = nullptr;

      KTextEditor::ConfigInterface* configView = nullptr;
      KTextEditor::ConfigInterface* configDoc = nullptr;

      QFont _initialFont;
      QFont _font;

      Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
      Q_PROPERTY(QColor selectionColor READ selectionColor WRITE setSelectionColor)
      Q_PROPERTY(QColor searchHighlightColor READ searchHighlightColor WRITE setSearchHighlightColor)
      Q_PROPERTY(QColor replaceHighlightColor READ replaceHighlightColor WRITE setReplaceHighlightColor)
      Q_PROPERTY(QColor iconBorderColor READ iconBorderColor WRITE setIconBorderColorColor)
      Q_PROPERTY(QColor foldingMarkerColor READ foldingMarkerColor WRITE setFoldingMarkerColor)
      Q_PROPERTY(QColor lineNumberColor READ lineNumberColor WRITE setLineNumberColor)
      Q_PROPERTY(QColor currentLineNumberColor READ currentLineNumberColor WRITE setCurrentLineNumberColor)
      Q_PROPERTY(QString scheme READ scheme WRITE setScheme)

    public:
      typedef KTextEditor::Cursor Cursor;
      typedef KTextEditor::Range Range;
      typedef KStandardAction::StandardAction StandardAction;
      typedef KTextEditor::View::ViewMode ViewMode;
      typedef KTextEditor::View::InputMode InputMode;
      typedef KTextEditor::SearchOptions SearchOptions;
      typedef KActionCollection ActionCollection;

      enum Action {
        DYNAMIC_WORD_WRAP,
        DYNAMIC_WORD_WRAP_INDICATOR,
        DYNAMIC_WORD_WRAP_MARKER,
        EXPORT_HTML,
        RELOAD,
        SAVE_AS_WITH_ENCODING,
        SHOW_NON_PRINTABLE_SPACES,
        ZOOM_IN,
        ZOOM_OUT
      };

    public:
      using TabviewWidget::TabviewWidget;

      explicit Document(QWidget* parent = nullptr);
      explicit Document(const QUrl&, QWidget* parent = nullptr);
      ~Document() override;

      QString name() const;
      QUrl url() const;
      QString path() const;

      /**
       * Get the current chosen encoding. The return value is an empty string,
       * if the document uses the default encoding of the editor and no own
       * special encoding.
       * \return current encoding of the document
       * \see setEncoding()
       */
      QString encoding() const;

      QString endOfLine() const;
      QString mimeType() const;
      QString mode() const;
      InputMode viewInputMode() const;
      QString viewInputModeHuman() const;
      ViewMode viewMode() const;
      QString viewModeHuman() const;
      Cursor cursorPosition() const;
      bool isModified() const;

      /**
       * @return true if the document is in readonly mode
       */
      bool isReadOnly() const;

      /**
       * Changes the behavior of the document to readonly or readwrite.
       * @param readwrite set to true to enable readwrite mode
       */
      void setWritable(bool readwrite = true);

      /**
       * Get the document font
       * @return The document font
       */
      const QFont& font() const;

      /**
       * Set the new document font
       * @param font The new document font
       */
      void setFont(const QFont& font);

      /**
       * Increase the document font size
       * @param step The increase step
       */
      void increaseFontSize(qreal step = 1.0);

      /**
       * Decrease the document font size
       * @param step The decrease step
       */
      void decreaseFontSize(qreal step = 1.0);

      /**
       * Reset the document font size
       */
      void resetFontSize();

      QList<KTextEditor::View*> views() const;

      QColor backgroundColor() const;
      QColor selectionColor() const;
      QColor searchHighlightColor() const;
      QColor replaceHighlightColor() const;
      QColor iconBorderColor() const;
      QColor foldingMarkerColor() const;
      QColor lineNumberColor() const;
      QColor currentLineNumberColor() const;
      QString scheme() const;

      QAction* action(Action) const;
      QAction* action(const StandardAction objectId) const;
      ActionCollection* actionCollection() const;

      /**
       * \brief Searches the given input range for a text pattern.
       *
       * Searches for a text pattern within the given input range.
       * The kind of search performed depends on the \p options
       * used. Use this function for plaintext searches as well as
       * regular expression searches. If no match is found the first
       * (and only) element in the vector return is the invalid range.
       * When searching for regular expressions, the first element holds
       * the range of the full match, the subsequent elements hold
       * the ranges of the capturing parentheses.
       *
       * \param range    Input range to search in
       * \param pattern  Text pattern to search for
       * \param options  Combination of search flags
       * \return         List of ranges (length >=1)
       */
      QVector<Range> searchText(const Range& range,
                                const QString& pattern,
                                const SearchOptions options = KTextEditor::Default) const;

      KTextEditor::View* createView(QWidget*);

      /**
       * Reload the current file.
       * The user will be prompted by the part on changes and more and can
       * cancel this action if it can harm.
       * \return \e true if the reload has been done, otherwise \e false.
       */
      bool reload();

    protected:
      void setDoc(KTextEditor::Document*);
      inline void disableViewActionShortcuts();

    signals:
      /**
       * This signal is emitted after a document has been saved to disk or for
       * remote files uploaded.
       */
      void saved(Document*);
      void cursorPositionChanged(Document*, const Cursor&);
      void viewModeChanged(Document*, const ViewMode&);
      void viewInputModeChanged(Document*, const QString&);
      void nameChanged(const QString&);
      void urlChanged(const QUrl&);
      void modifiedChanged(Document*);

    public slots:
      void setBackgroundColor(const QColor&);
      void setSelectionColor(const QColor&);
      void setSearchHighlightColor(const QColor&);
      void setReplaceHighlightColor(const QColor&);
      void setIconBorderColorColor(const QColor&);
      void setFoldingMarkerColor(const QColor&);
      void setLineNumberColor(const QColor&);
      void setCurrentLineNumberColor(const QColor&);
      void setScheme(const QString&);

      /**
       * Set the encoding for this document. This encoding will be used
       * while loading and saving files, it will \e not affect the already
       * existing content of the document, e.g. if the file has already been
       * opened without the correct encoding, this will \e not fix it, you
       * would for example need to trigger a reload for this.
       * \param encoding new encoding for the document, the name must be
       *        accepted by QTextCodec, if an empty encoding name is given, the
       *        part should fallback to its own default encoding, e.g. the
       *        system encoding or the global user settings
       * \return \e true on success, or \e false, if the encoding could not be set.
       * \see encoding()
       */
      bool setEncoding(const QString&);

      void openFile(const QUrl&);
      void saveFile();
      void saveFileAs(const QString&);

      void close() override;
      void closeFile();

    protected:
      static KTextEditor::Document* document(const QUrl& url);
  };
}  // namespace CodeLineX

#endif  // HPXTABVIEW_COMPONENTS_DOCUMENT_H_
