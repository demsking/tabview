/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HPXTABVIEW_COMPONENTS_TABVIEW_H_
#define HPXTABVIEW_COMPONENTS_TABVIEW_H_

#include <QFrame>

#include "global.h"  // NOLINT(build/include)

class QWidget;
class QVBoxLayout;
class QStackedWidget;

namespace HPX {
  class TabviewBar;
  class TabviewWidget;
  class TabviewModel;

  class LIBHPXTABVIEW_EXPORT Tabview: public QFrame {
    Q_OBJECT

    QVBoxLayout* _mainLayout = nullptr;
    TabviewBar* _tabviewBar = nullptr;
    QStackedWidget* _stackedWidget = nullptr;
    TabviewModel* _model = nullptr;

    bool _focused = false;
    bool _enableCtrlTabNavigation = true;

    public:
      explicit Tabview(QWidget* parent = nullptr);
      ~Tabview();

      TabviewBar* tabviewBar() const;

      TabviewModel* model() const;
      void setModel(TabviewModel*);

      void enableCtrlTabNavigation();
      void disableCtrlTabNavigation();

      TabviewWidget* widget(int index) const;
      TabviewWidget* currentWidget() const;

      int count() const;
      bool isEmpty() const;
      int indexOf(TabviewWidget*);
      int currentIndex() const;

    public slots:
      void setCurrentIndex(int index);
      void setCurrentWidget(TabviewWidget*);
      void setFocus(bool focused);

    public:
      bool next(int pos, const QModelIndex&);
      bool nextPositon(int pos);
      bool previousPositon(int pos);

    protected:
      inline void connectWidget(TabviewWidget*);

    protected slots:
      void onDataChange(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles);

    protected:
      virtual bool eventFilter(QObject* watched, QEvent* event);

    signals:
      void focusIn();
      void focusOut();
      void currentChanged(int index);
  };
}  // namespace HPX

#endif // HPXTABVIEW_COMPONENTS_TABVIEW_H_
