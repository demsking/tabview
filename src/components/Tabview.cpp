/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/Tabview.h"

#include <QStyle>
#include <QEvent>
#include <QWidget>
#include <QKeyEvent>
#include <QVBoxLayout>
#include <QStackedWidget>
#include <QCoreApplication>

#include "components/TabviewBar.h"
#include "components/TabviewWidget.h"

#include "models/TabviewItem.h"
#include "models/TabviewModel.h"

namespace HPX {
  Tabview::Tabview(QWidget* parent): QFrame(parent) {
    setProperty("Tabview", true);

    _mainLayout = new QVBoxLayout(this);
    _tabviewBar = new TabviewBar(this);
    _stackedWidget = new QStackedWidget(this);

    _mainLayout->setMargin(0);
    _mainLayout->setSpacing(0);
    _mainLayout->addWidget(_tabviewBar);
    _mainLayout->addWidget(_stackedWidget);

    _stackedWidget->setProperty("TabviewContainer", true);

    connect(_tabviewBar, &TabviewBar::currentChanged, _stackedWidget, &QStackedWidget::setCurrentIndex);
    connect(_stackedWidget, &QStackedWidget::currentChanged, this, &Tabview::currentChanged);
    connect(_stackedWidget, &QStackedWidget::currentChanged, this, [this](int pos) {
      QWidget* widget = _stackedWidget->widget(pos);

      if (widget) {
        setFocusProxy(widget);
        widget->setFocus();
      }
    });

    QCoreApplication::instance()->installEventFilter(this);
  }

  Tabview::~Tabview() {
    delete _stackedWidget;
    delete _tabviewBar;
    delete _mainLayout;
  }

  TabviewBar* Tabview::tabviewBar() const {
    return _tabviewBar;
  }

  TabviewModel* Tabview::model() const {
    return _model;
  }

  void Tabview::setModel(TabviewModel* model) {
    if (_model != nullptr) {
      _model->disconnect(this);
    }

    _model = model;

    _tabviewBar->setModel(model);

    connect(model, &TabviewModel::rowsInserted, this, [this](const QModelIndex& parent, int first, int last) {
      Q_UNUSED(parent)

      for (int i = first; i <= last; i++) {
        TabviewWidget* widget = _model->widget(i);

        _stackedWidget->insertWidget(i, widget);
        connectWidget(widget);
      }

      setCurrentIndex(last);
    });

    connect(model, &TabviewModel::rowsAboutToBeRemoved, this, [this](const QModelIndex& parent, int first, int last) {
      Q_UNUSED(parent)

      for (int i = last; i >= first; i--) {
        TabviewWidget* widget = _model->widget(i);

        widget->disconnect(this);
        _stackedWidget->removeWidget(widget);
      }
    });

    connect(model, &TabviewModel::rowsRemoved, this, [this](const QModelIndex& parent, int first, int last) {
      Q_UNUSED(parent)
      Q_UNUSED(first)
      Q_UNUSED(last)

      if (_tabviewBar->currentIndex() > -1) {
        setCurrentIndex(_tabviewBar->currentIndex());
      }
    });

    connect(model, &TabviewModel::dataChanged, this, &Tabview::onDataChange);
  }

  void Tabview::enableCtrlTabNavigation() {
    _enableCtrlTabNavigation = true;
  }

  void Tabview::disableCtrlTabNavigation() {
    _enableCtrlTabNavigation = false;
  }

  TabviewWidget* Tabview::widget(int index) const {
    return _model->widget(index);
  }

  TabviewWidget* Tabview::currentWidget() const {
    return widget(currentIndex());
  }

  int Tabview::count() const {
    return _model->rowCount();
  }

  bool Tabview::isEmpty() const {
    return count() == 0;
  }

  int Tabview::indexOf(TabviewWidget* widget) {
    return _stackedWidget->indexOf(widget);
  }

  int Tabview::currentIndex() const {
    return _stackedWidget->currentIndex();
  }

  void Tabview::setCurrentIndex(int index) {
    _tabviewBar->setCurrentIndex(index);
  }

  void Tabview::setCurrentWidget(TabviewWidget* widget) {
    const int index = indexOf(widget);

    if (index > -1) {
      setCurrentIndex(index);
    }
  }

  void Tabview::setFocus(bool focused) {
    _focused = focused;
    setProperty("focused", focused);

    style()->unpolish(this);
    style()->polish(this);

    currentWidget()->style()->unpolish(this);
    currentWidget()->style()->polish(this);

    if (focused) {
      emit focusIn();
    } else {
      emit focusOut();
    }
  }

  bool Tabview::next(int pos, const QModelIndex& index) {
    if (index.isValid()) {
      const int next = index.row();

      if (next != pos) {
        setCurrentIndex(next);
        _model->widget(next)->setFocus();

        return true;
      }
    }

    return false;
  }

  bool Tabview::nextPositon(int pos) {
    const QModelIndex& index = _model->sibling(pos + 1, 0);

    return next(pos, index);
  }

  bool Tabview::previousPositon(int pos) {
    const QModelIndex& index = _model->sibling(pos - 1, 0);

    return next(pos, index);
  }

  void Tabview::connectWidget(TabviewWidget* widget) {
    connect(widget, &TabviewWidget::focusIn, this, [this, widget]() {
      const int pos = _stackedWidget->indexOf(widget);

      if (pos != currentIndex()) {
        setCurrentIndex(pos);
      }

      setFocus(true);
    });

    connect(widget, &TabviewWidget::focusOut, this, [this]() {
      setFocus(false);
    });

    connect(widget, &TabviewWidget::closed, this, [this, widget]() {
      const int pos = _stackedWidget->indexOf(widget);

      _model->removeRow(pos);
    });
  }

  void Tabview::onDataChange(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles) {
    Q_UNUSED(bottomRight)

    if (!topLeft.isValid()) {
      return;
    }

    for (const int& role: roles) {
      if (static_cast<TabviewItem::Role>(role) == TabviewItem::Role::WidgetRole) {
        const int pos = topLeft.row();
        QWidget* widget = _stackedWidget->widget(pos);
        TabviewWidget* newWidget = _model->widget(topLeft);
        const int currentPos = currentIndex();

        widget->disconnect(this);

        _stackedWidget->insertWidget(pos, newWidget);
        _stackedWidget->setCurrentIndex(pos + 1);
        _stackedWidget->removeWidget(widget);
        _stackedWidget->setCurrentIndex(currentPos);

        connectWidget(newWidget);
      }
    }
  }

  bool Tabview::eventFilter(QObject* watched, QEvent* event) {
    if (_focused && event->type() == QEvent::KeyPress) {
      QKeyEvent* keyPress = static_cast<QKeyEvent*>(event);

      if (keyPress->modifiers() & Qt::AltModifier) {
        if (keyPress->key() == Qt::Key_Right) {
          nextPositon(currentIndex());

          return true;
        } else if (keyPress->key() == Qt::Key_Left) {
          previousPositon(currentIndex());

          return true;
        }
      } else if (keyPress->modifiers() & Qt::ControlModifier) {
        if (_enableCtrlTabNavigation) {
          if (keyPress->modifiers() & Qt::ShiftModifier) {
            if (keyPress->key() == Qt::Key_Backtab) {
              if (!previousPositon(currentIndex())) {
                previousPositon(_model->rowCount());
              }

              return true;
            }
          } else if (keyPress->key() == Qt::Key_Tab) {
            if (!nextPositon(currentIndex())) {
              nextPositon(-1);
            }

            return true;
          }
        } else if (keyPress->key() == Qt::Key_W) {
          if (!isEmpty()) {
            currentWidget()->close();
          }

          return true;
        }
      }
    }

    return QFrame::eventFilter(watched, event);
  }
}  // namespace HPX
