/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/TabviewBarButton.h"

#include <QStyle>
#include <QLabel>
#include <QFrame>
#include <QWidget>
#include <QHBoxLayout>
#include <QMouseEvent>
#include <QPushButton>

namespace HPX {
  TabviewBarButton::TabviewBarButton(QWidget* parent): QFrame(parent) {
    setProperty("TabviewBarButton", true);
    setFocusPolicy(Qt::NoFocus);

    _layout = new QHBoxLayout(this);

    _textLabel = new QLabel(this);
    _textLabel->setProperty("TabviewBarButtonText", true);
    _textLabel->setAlignment(Qt::AlignCenter | Qt::AlignVCenter | Qt::AlignHCenter);

    _closeButton = new QPushButton(this);

    _closeButton->setIcon(QIcon::fromTheme(QStringLiteral("tabview-close")));
    _closeButton->setProperty("TabviewBarButtonClose", true);
    _closeButton->setFocusPolicy(Qt::NoFocus);
    _closeButton->setToolTip(tr("Close"));

    _layout->setMargin(0);
    _layout->addWidget(_textLabel);
    _layout->addWidget(_closeButton);

    connect(_closeButton, &QPushButton::clicked, this, &TabviewBarButton::closeButtonClicked);
  }

  TabviewBarButton::~TabviewBarButton() {
    delete _textLabel;
    delete _layout;
    delete _closeButton;
  }

  void TabviewBarButton::setText(const QString& text) {
    const QFontMetrics metrics(font());
    const QString elidedText = metrics.elidedText(text, Qt::ElideRight, 300);

    _textLabel->setText(elidedText);
  }

  QString TabviewBarButton::text() const {
    return _textLabel->text();
  }

  void TabviewBarButton::unpolish() {
    style()->unpolish(this);
    style()->unpolish(_textLabel);
    style()->unpolish(_closeButton);
  }

  void TabviewBarButton::polish() {
    style()->polish(this);
    style()->polish(_textLabel);
    style()->polish(_closeButton);
  }

  void TabviewBarButton::mousePressEvent(QMouseEvent*) {
    _pressed = true;
  }

  void TabviewBarButton::mouseReleaseEvent(QMouseEvent*) {
    if (_pressed) {
      emit clicked();
    }

    _pressed = false;
  }
}  // namespace HPX
