/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HPXTABVIEW_COMPONENTS_TOOLBOX_H_
#define HPXTABVIEW_COMPONENTS_TOOLBOX_H_

#include <QFrame>

#include "global.h"  // NOLINT(build/include)

class QWidget;
class QHBoxLayout;
class QAbstractButton;

namespace HPX {
  class LIBHPXTABVIEW_EXPORT ToolBox: public QFrame {
    QHBoxLayout* _layout = nullptr;

    public:
      explicit ToolBox(QWidget* parent = nullptr);
      ~ToolBox();

      void addButton(QAbstractButton*);
      void removeButton(QAbstractButton*);
  };
}  // namespace HPX

#endif  // HPXTABVIEW_COMPONENTS_TOOLBOX_H_
