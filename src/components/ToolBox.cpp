/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/ToolBox.h"

#include <QWidget>
#include <QVariant>
#include <QHBoxLayout>
#include <QAbstractButton>

namespace HPX {
  ToolBox::ToolBox(QWidget* parent): QFrame(parent) {
    _layout = new QHBoxLayout;

    _layout->setSpacing(0);
    _layout->setMargin(0);
    _layout->setContentsMargins(QMargins());
    _layout->setAlignment(Qt::AlignRight);

    setProperty("ToolBox", true);
    setLayout(_layout);
  }

  ToolBox::~ToolBox() {
    delete _layout;
  }

  void ToolBox::addButton(QAbstractButton* button) {
    button->setProperty("ToolBoxButton", true);

    _layout->addWidget(button);
  }

  void ToolBox::removeButton(QAbstractButton* button) {
    _layout->removeWidget(button);
  }
}  // namespace HPX
