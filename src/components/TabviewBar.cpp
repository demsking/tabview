/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/TabviewBar.h"

#include <QTimer>
#include <QWidget>
#include <QScrollBar>
#include <QScrollArea>
#include <QHBoxLayout>
#include <QPushButton>

#include "components/TabviewWidget.h"
#include "components/TabviewBarButton.h"
#include "components/ToolBox.h"

#include "models/TabviewItem.h"
#include "models/TabviewModel.h"

#define SCROLL_STEP 50

namespace HPX {
  TabviewBar::TabviewBar(QWidget* parent): QFrame(parent) {
    setFixedHeight(33);
    setProperty("TabviewBar", true);
    setFocusPolicy(Qt::NoFocus);

    _mainLayout = new QHBoxLayout(this);
    _buttonsLayout = new QHBoxLayout;
    _buttonsFrame = new QFrame(this);
    _scrollArea = new QScrollArea(this);

    _toolboxLayout = new QHBoxLayout;
    _scrollButtonsToolbox = new ToolBox(this);
    _scrollLeftButton = new QPushButton(this);
    _scrollRightButton = new QPushButton(this);

    _buttonsLayout->setSpacing(0);
    _buttonsLayout->setMargin(0);
    _buttonsLayout->setContentsMargins(QMargins());
    _buttonsLayout->setAlignment(Qt::AlignLeft);

    _toolboxLayout->setSpacing(0);
    _toolboxLayout->setMargin(0);
    _toolboxLayout->setContentsMargins(QMargins());
    _toolboxLayout->setAlignment(Qt::AlignRight);
    _toolboxLayout->addWidget(_scrollButtonsToolbox);

    _mainLayout->setSpacing(0);
    _mainLayout->setMargin(0);
    _mainLayout->addWidget(_scrollArea);
    _mainLayout->addLayout(_toolboxLayout);

    _buttonsFrame->setLayout(_buttonsLayout);
    _buttonsFrame->setProperty("TabviewBarFrame", true);

    _scrollArea->setWidget(_buttonsFrame);
    _scrollArea->setWidgetResizable(true);
    _scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    _scrollLeftButton->setIcon(QIcon::fromTheme(QStringLiteral("tabview-previous")));
    _scrollLeftButton->setToolTip(tr("Scroll Left"));
    _scrollLeftButton->setProperty("TabviewBarScrollLeftButton", true);
    connect(_scrollLeftButton, &QPushButton::pressed, this, &TabviewBar::scrollLeft);

    _scrollRightButton->setIcon(QIcon::fromTheme(QStringLiteral("tabview-next")));
    _scrollRightButton->setToolTip(tr("Scroll Right"));
    _scrollRightButton->setProperty("TabviewBarScrollRightButton", true);
    connect(_scrollRightButton, &QPushButton::pressed, this, &TabviewBar::scrollRight);

    _scrollButtonsToolbox->hide();
    _scrollButtonsToolbox->addButton(_scrollLeftButton);
    _scrollButtonsToolbox->addButton(_scrollRightButton);

    connect(_scrollArea->horizontalScrollBar(), &QScrollBar::valueChanged,
            this, &TabviewBar::onScrollBarValueChange);

    connect(_scrollArea->horizontalScrollBar(), &QScrollBar::rangeChanged,
            this, &TabviewBar::onScrollBarRangeChange);
  }

  TabviewBar::~TabviewBar() {
    for (TabviewBarButton* button: _buttons) {
      delete button;
    }

    delete _scrollRightButton;
    delete _scrollLeftButton;
    delete _scrollButtonsToolbox;
    delete _toolboxLayout;
    delete _buttonsLayout;
    delete _buttonsFrame;
    delete _scrollArea;
    delete _mainLayout;
  }

  void TabviewBar::addToolBox(ToolBox* toolbox) {
    _toolboxLayout->addWidget(toolbox);
  }

  void TabviewBar::removeToolBox(ToolBox* toolbox) {
    _toolboxLayout->removeWidget(toolbox);
  }

  ToolBox* TabviewBar::scrollButtonsToolbox() const {
    return _scrollButtonsToolbox;
  }

  TabviewModel* TabviewBar::model() const {
    return _model;
  }

  void TabviewBar::setModel(TabviewModel* model) {
    if (_model != nullptr) {
      _model->disconnect(this);
    }

    _model = model;

    connect(model, &TabviewModel::rowsInserted, this, &TabviewBar::onRowsInsert);
    connect(model, &TabviewModel::rowsRemoved, this, &TabviewBar::onRowsRemove);
    connect(model, &TabviewModel::dataChanged, this, &TabviewBar::onDataChange);
  }

  TabviewBarButton* TabviewBar::tabButton(int index) const {
    return index < _buttons.size() ? _buttons[index] : nullptr;
  }

  TabviewBarButton* TabviewBar::currentTabButton() const {
    return _currentIndex < 0 ? nullptr : _buttons[_currentIndex];
  }

  int TabviewBar::currentIndex() const {
    return _currentIndex;
  }

  void TabviewBar::setCurrentIndex(int index) {
    if (index < 0 || index >= _buttons.size()) {
      throw std::out_of_range("Out Of Range Exception");
    }

    if (_currentIndex > -1 && _currentIndex < _buttons.size()) {
      setSelectedButton(_buttons[_currentIndex], false);
    }

    _currentIndex = index;

    TabviewBarButton* button = _buttons[_currentIndex];

    setFocusProxy(button);
    setSelectedButton(button, true);
    ensureCurrentButtonVisible();

    emit currentChanged(index);
  }

  void TabviewBar::setSelectedButton(TabviewBarButton* button, bool selected) {
    button->setProperty("selected", selected);

    button->unpolish();
    button->polish();
  }

  void TabviewBar::updateButtonsPositionProperty() {
    for (int i = 0, n = _buttons.size() - 1; i <= n; i++) {
      TabviewBarButton* button = _buttons[i];

      button->setProperty("index", i);
      button->setProperty("first", i == 0);
      button->setProperty("last", i == n);

      button->unpolish();
      button->polish();
    }
  }

  void TabviewBar::scrollLeft() {
    QScrollBar* horizontalScrollBar = _scrollArea->horizontalScrollBar();

    const int value = horizontalScrollBar->value();
    const int nextValue = value - SCROLL_STEP;

    QWidget* widget = _scrollArea->childAt(1, 0);
    TabviewBarButton* button = qobject_cast<TabviewBarButton*>(widget);

    horizontalScrollBar->setValue(nextValue);

    if (button) {
      const int index = _buttons.indexOf(button) - 1;

      _scrollArea->ensureWidgetVisible(button, 0);

      if (index > -1) {
        _scrollArea->ensureWidgetVisible(_buttons[index], 0);
      }
    }

    onScrollBarValueChange(horizontalScrollBar->value());
  }

  void TabviewBar::scrollRight() {
    QScrollBar* horizontalScrollBar = _scrollArea->horizontalScrollBar();

    const int value = horizontalScrollBar->value();
    const int nextValue = value + SCROLL_STEP;

    QWidget* widget = _scrollArea->childAt(horizontalScrollBar->maximum() - 1, 0);
    TabviewBarButton* button = qobject_cast<TabviewBarButton*>(widget);

    horizontalScrollBar->setValue(nextValue);

    if (button) {
      const int index = _buttons.indexOf(button) + 1;

      _scrollArea->ensureWidgetVisible(button, 0);

      if (index < _buttons.size()) {
        _scrollArea->ensureWidgetVisible(_buttons[index], 0);
      }
    }

    onScrollBarValueChange(horizontalScrollBar->value());
  }

  void TabviewBar::ensureCurrentButtonVisible() {
    _scrollArea->ensureWidgetVisible(currentTabButton(), 0, 0);
    onScrollBarValueChange(_scrollArea->horizontalScrollBar()->value());
  }

  void TabviewBar::insertButton(const QModelIndex& index, int pos) {
    const QString name = _model->title(index);
    const QString toolTip = _model->toolTip(index);
    TabviewBarButton* button = new TabviewBarButton(this);

    button->setText(name);
    button->setToolTip(toolTip);

    connect(button, &TabviewBarButton::clicked, this, [this, button]() {
      const int pos = _buttons.indexOf(button);

      if (pos != currentIndex()) {
        setCurrentIndex(pos);
      }

      _model->widget(pos)->setFocus();
    });

    connect(button, &TabviewBarButton::closeButtonClicked, this, [this, button]() {
      const int pos = _buttons.indexOf(button);

      _model->removeRow(pos);
    });

    _buttons.insert(pos, button);
    _buttonsLayout->insertWidget(pos, button);
  }

  void TabviewBar::onScrollBarValueChange(int value) {
    QScrollBar* horizontalScrollBar = _scrollArea->horizontalScrollBar();

    _scrollLeftButton->setDisabled(value <= 1);
    _scrollRightButton->setDisabled(value >= horizontalScrollBar->maximum() - 1);
  }

  void TabviewBar::onScrollBarRangeChange(int min, int max) {
    Q_UNUSED(min)

    _scrollButtonsToolbox->setHidden(max == 0);
  }

  void TabviewBar::onRowsInsert(const QModelIndex& parent, int first, int last) {
    Q_UNUSED(parent)

    for (int i = first; i <= last; i++) {
      const QModelIndex index = _model->index(i, 0, parent);

      insertButton(index, i);
    }

    setCurrentIndex(last);
    updateButtonsPositionProperty();
    ensureCurrentButtonVisible();

    QTimer::singleShot(500, this, &TabviewBar::ensureCurrentButtonVisible);
  }

  void TabviewBar::onRowsRemove(const QModelIndex& parent, int first, int last) {
    for (int i = last; i >= first; i--) {
      TabviewBarButton* button = _buttons.takeAt(i);

      _buttonsLayout->removeWidget(button);

      delete button;
    }

    const int step = first <= _currentIndex ? -1 : 0;
    const QModelIndex& index = _model->sibling(_currentIndex + step, 0, parent);

    if (index.isValid()) {
      setCurrentIndex(index.row());
      updateButtonsPositionProperty();
    } else {
      _currentIndex = -1;
    }
  }

  void TabviewBar::onDataChange(const QModelIndex& topLeft,
                                const QModelIndex& bottomRight,
                                const QVector<int>& roles) {
    Q_UNUSED(bottomRight)

    if (!topLeft.isValid()) {
      return;
    }

    const int pos = topLeft.row();
    TabviewBarButton* button = _buttons[pos];

    for (const int& role: roles) {
      switch (static_cast<TabviewItem::Role>(role)) {
        case TabviewItem::Role::TitleRole:
          button->setText(_model->title(topLeft));
          break;

        case TabviewItem::Role::ToolTipRole:
          button->setToolTip(_model->toolTip(topLeft));
          break;

        default:
          break;
      }
    }
  }
}  // namespace HPX
