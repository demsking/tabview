/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HPXTABVIEW_COMPONENTS_TABVIEWBAR_H_
#define HPXTABVIEW_COMPONENTS_TABVIEWBAR_H_

#include <QFrame>

#include "global.h"  // NOLINT(build/include)

class QWidget;
class QScrollArea;
class QHBoxLayout;
class QPushButton;

namespace HPX {
  class TabviewModel;
  class TabviewWidget;
  class TabviewBarButton;
  class ToolBox;

  class LIBHPXTABVIEW_EXPORT TabviewBar: public QFrame {
    Q_OBJECT

    QHBoxLayout* _mainLayout = nullptr;
    QHBoxLayout* _buttonsLayout = nullptr;
    QFrame* _buttonsFrame = nullptr;
    QScrollArea* _scrollArea = nullptr;
    TabviewModel* _model = nullptr;
    QList<TabviewBarButton*> _buttons;
    int _currentIndex = -1;

    QHBoxLayout* _toolboxLayout = nullptr;
    ToolBox* _scrollButtonsToolbox = nullptr;
    QPushButton* _scrollLeftButton = nullptr;
    QPushButton* _scrollRightButton = nullptr;

    public:
      explicit TabviewBar(QWidget* parent = nullptr);
      ~TabviewBar();

      void addToolBox(ToolBox*);
      void removeToolBox(ToolBox*);

      ToolBox* scrollButtonsToolbox() const;

      TabviewModel* model() const;
      void setModel(TabviewModel*);

      TabviewBarButton* tabButton(int index) const;
      TabviewBarButton* currentTabButton() const;

      int currentIndex() const;

    public slots:
      void setCurrentIndex(int index);

    protected:
      inline void setSelectedButton(TabviewBarButton*, bool selected);
      inline void updateButtonsPositionProperty();
      inline void ensureCurrentButtonVisible();

    protected:
      inline void insertButton(const QModelIndex&, int pos);

    public slots:
      void scrollLeft();
      void scrollRight();

    protected slots:
      void onScrollBarValueChange(int value);
      void onScrollBarRangeChange(int min, int max);
      void onRowsInsert(const QModelIndex& parent, int first, int last);
      void onRowsRemove(const QModelIndex& parent, int first, int last);
      void onDataChange(const QModelIndex& topLeft,
                        const QModelIndex& bottomRight,
                        const QVector<int>& roles);

    signals:
      void currentChanged(int index);
  };
}  // namespace HPX

#endif  // HPXTABVIEW_COMPONENTS_TABVIEWBAR_H_
