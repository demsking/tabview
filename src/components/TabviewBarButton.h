/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HPXTABVIEW_COMPONENTS_TABVIEWBARBUTTON_H_
#define HPXTABVIEW_COMPONENTS_TABVIEWBARBUTTON_H_

#include <QFrame>

#include "global.h"  // NOLINT(build/include)

class QLabel;
class QWidget;
class QHBoxLayout;
class QMouseEvent;
class QPushButton;

namespace HPX {
  class TabviewBarButtonClose;

  class LIBHPXTABVIEW_EXPORT TabviewBarButton: public QFrame {
    Q_OBJECT

    QHBoxLayout* _layout = nullptr;
    QLabel* _textLabel = nullptr;
    QPushButton* _closeButton = nullptr;
    bool _pressed = false;

    public:
      explicit TabviewBarButton(QWidget* parent = nullptr);
      ~TabviewBarButton() override;

      void setText(const QString&);
      QString text() const;

      void unpolish();
      void polish();

    signals:
      void clicked();
      void closeButtonClicked();

    protected:
      void mousePressEvent(QMouseEvent*) override;
      void mouseReleaseEvent(QMouseEvent*) override;
  };
}  // namespace HPX

#endif  // HPXTABVIEW_COMPONENTS_TABVIEWBARBUTTON_H_
