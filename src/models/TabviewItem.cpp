/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "models/TabviewItem.h"
#include "models/StandarItemType.h"

#include "components/TabviewWidget.h"

namespace HPX {
  int TabviewItem::type() const {
    return StandarItemType::Tabview;
  }

  QString TabviewItem::title() const {
    return data(Role::TitleRole).value<QString>();
  }

  void TabviewItem::setTitle(const QString& title) {
    setData(title, Role::TitleRole);
  }

  QString TabviewItem::toolTip() const {
    return data(Role::ToolTipRole).value<QString>();
  }

  void TabviewItem::setToolTip(const QString& toolTip) {
    setData(toolTip, Role::ToolTipRole);
  }

  TabviewWidget* TabviewItem::widget() const {
    return data(Role::WidgetRole).value<TabviewWidget*>();
  }

  void TabviewItem::setWidget(TabviewWidget* widget) {
    setData(QVariant::fromValue(widget), Role::WidgetRole);
  }
} // namespace HPX
