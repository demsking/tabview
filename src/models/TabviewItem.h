/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HPXTABVIEW_MODELS_TABVIEWITEM_H_
#define HPXTABVIEW_MODELS_TABVIEWITEM_H_

#include <QStandardItem>

#include "global.h"  // NOLINT(build/include)

namespace HPX {
  class TabviewWidget;

  class LIBHPXTABVIEW_EXPORT TabviewItem: public QStandardItem {
    public:
      enum Role {
        TitleRole = Qt::UserRole + 8099,
        ToolTipRole,
        WidgetRole
      };

      TabviewItem() = default;

      int type() const override;

      QString title() const;
      void setTitle(const QString& title);

      QString toolTip() const;
      void setToolTip(const QString& toolTip);

      TabviewWidget* widget() const;
      void setWidget(TabviewWidget*);
  };
} // namespace HPX

#endif // HPXTABVIEW_MODELS_TABVIEWITEM_H_
