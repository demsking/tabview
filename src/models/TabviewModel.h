/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HPXTABVIEW_MODELS_TABVIEWMODEL_H_
#define HPXTABVIEW_MODELS_TABVIEWMODEL_H_

#include <QStandardItemModel>

#include "global.h"  // NOLINT(build/include)

namespace HPX {
  class TabviewItem;
  class TabviewWidget;

  class LIBHPXTABVIEW_EXPORT TabviewModel: public QStandardItemModel {
    Q_OBJECT

    public:
      explicit TabviewModel(QObject* parent = nullptr);

      void add(TabviewItem*);
      int count() const;

      QString title(int pos) const;
      QString title(const QModelIndex&) const;

      QString toolTip(int pos) const;
      QString toolTip(const QModelIndex&) const;

      TabviewWidget* widget(int pos) const;
      TabviewWidget* widget(const QModelIndex&) const;

      QModelIndex sibling(int row, int column, const QModelIndex& = QModelIndex()) const override;
  };
} // namespace HPX

#endif // HPXTABVIEW_MODELS_TABVIEWMODEL_H_
