/**
 * HPX Tabview - A fully stylized TabsView implementation
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/TabviewWidget.h"

#include "models/TabviewModel.h"
#include "models/TabviewItem.h"

namespace HPX {
  TabviewModel::TabviewModel(QObject* parent): QStandardItemModel(parent) {
    setItemRoleNames({
      { TabviewItem::TitleRole, "title" },
      { TabviewItem::ToolTipRole, "toolTip" },
      { TabviewItem::WidgetRole, "widget" }
    });
  }

  void TabviewModel::add(TabviewItem* item) {
    appendRow(item);
  }

  int TabviewModel::count() const {
    return rowCount();
  }

  QString TabviewModel::title(int pos) const {
    return title(index(pos, 0));
  }

  QString TabviewModel::title(const QModelIndex& index) const {
    return data(index, TabviewItem::TitleRole).value<QString>();
  }

  QString TabviewModel::toolTip(int pos) const {
    return toolTip(index(pos, 0));
  }

  QString TabviewModel::toolTip(const QModelIndex& index) const {
    return data(index, TabviewItem::ToolTipRole).value<QString>();
  }

  TabviewWidget* TabviewModel::widget(int pos) const {
    return widget(index(pos, 0));
  }

  TabviewWidget* TabviewModel::widget(const QModelIndex& index) const {
    return data(index, TabviewItem::WidgetRole).value<TabviewWidget*>();
  }

  QModelIndex TabviewModel::sibling(int row, int column, const QModelIndex& idx) const {
    if (rowCount() == 0) {
      return QStandardItemModel::sibling(row, column, idx);
    }

    if (row >= rowCount()) {
      return sibling(row - 1, column, idx);
    }

    if (row < 0) {
      return sibling(row + 1, column, idx);
    }

    return QStandardItemModel::sibling(row, column, idx);
  }
}  // namespace HPX
